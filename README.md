# arch-wiki-lite

This project makes the [Arch Wiki][arch-wiki] accessible and portable. The
existing `arch-wiki-docs` package is a simple unorganized dump of html files,
while `arch-wiki-lite` goes a few steps further:

* extremely fast search engine (with regex support and ranking)
* console viewer (with highlighting of links and regex matches)
* language filtering (with a summary of languages by page count)
* 1/9th the size

arch-wiki-lite is designed to offer the smoothest possible experience for the
poor person stuck without internet access or any way of starting a graphical
web browser.

Here is a sample run:

```sh
$ wiki-search suspend

┌───────────────────────────────────────────────────┐
│ Select a wiki page:                               │
│ ┌───────────────────────────────────────────────┐ │
│ │   Pm-utils                                    │ │
│ │   Hibernate-script                            │ │
│ │   Power management                            │ │
│ │   Uswsusp                                     │ │
│ │   Power management/Suspend and hibernate      │ │
│ │   PulseAudio/Troubleshooting                  │ │
│ │   TuxOnIce                                    │ │
│ │   Chrome OS devices                           │ │
│ │   Laptop/HP                                   │ │
│ │   MacBook                                     │ │
│ └───────────────────────────────────────────────┘ │
│                                                   │
│                                                   │
├───────────────────────────────────────────────────┤
│             <  OK  >       <Cancel>               │
└───────────────────────────────────────────────────┘
```

Select an entry and it will pop up in less or $PAGER. If you don't have dialog
installed then it will fall back on the old console UI:

```sh
$ wiki-search suspend
Now choose a page with wiki-search [number]
0  Pm-utils                                00002046
1  Hibernate-script                        00002490
2  Power management                        00001176
3  Uswsusp                                 00001711
[continues]

$ wiki-search 1
[page #1 pops up in less or $PAGER]
```

By default wiki-search filters to the english-language pages, but all the
other languages are there too:

```sh
$ wiki-search --lang
    2129 en
     525 zh-CN
     507 ru
     427 es
     356 it
     158 cs
     131 zh-TW
     103 pt
[continues]

$ export wiki_lang="it"
$ wiki-search xorg
0  it/AMD Catalyst                        00000106
1  it/Xorg                                00000009
2  it/NVIDIA                              00000075
3  it/Touchpad Synaptics                  00000112
[continues]
```

The results are sorted by how many times the search term was found in the page.

If you really like the local search but want to view the wiki pages in your
browser, there is the wiki-search-html command. Other than how the page is
rendered it operates exactly the same.

The content of arch-wiki-lite is identical to arch-wiki-docs.

## License

* [GPLv2][LICENSE-GPL2] for the code.
* [GFDL-1.3-or-later][LICENSE-GFDL] for usage of upstream documentation.

[arch-wiki]: https://wiki.archlinux.org
[LICENSE-GPL2]: LICENSE.GPL2
[LICENSE-GFDL]: LICENSE.GFDL-1.3-or-later

